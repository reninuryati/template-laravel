@extends('adminlte.master')
@section('title', 'Edit Pertanyaan')
@section('content')
    <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
    <label for="title">Judul</label>
    <input type="text" class="form-control" name ="judul" id="title" value="{{$pertanyaan->judul}}" placeholder="Masukkan judul">
    @error('title')
            <div class="alert alert-danger">
                {{$message}}
            </div>
    @enderror
    </div>
    <div class="form-group">
    <label for="body">Isi</label>
    <textarea name="isi" id="body" class="form-control" cols="38" rows="10"></textarea>
        @error('body')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection