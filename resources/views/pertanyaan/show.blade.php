@extends('adminlte.master')
@section('title', 'Show Pertanyaan')
@section('content')
    <div class="mx-3">
        <h4>{{$pertanyaan->judul}}</h4>
        <p>{{$pertanyaan->isi}}</p>
        <a href="/pertanyaan" class="btn btn-danger">back</a>
    </div>
@endsection